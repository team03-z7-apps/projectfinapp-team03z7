package com.example.bankapp_g03_z07.interfaz;

import com.example.bankapp_g03_z07.model.Lugar;

public interface RepositorioLugares {

    Lugar elemento(int id);
    void agregar(Lugar lugar);
    void eliminar(int id);
    void actualizar(int id, Lugar lugar);
    int nuevo();
    int cantidad();
}
