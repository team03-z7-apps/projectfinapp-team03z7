package com.example.bankapp_g03_z07.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import com.example.bankapp_g03_z07.model.GeoPunto;
import com.example.bankapp_g03_z07.model.Lugar;

import java.util.ArrayList;

public class DbLugares extends DbHelper{
    Context context;

    public DbLugares(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public long insertaLugar(String nombre, String direccion, String horario, double longitud, double latitud, String tipolugar){
        long id = 0;
        try {
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nombre", nombre);
            values.put("direccion", direccion);
            values.put("horario", horario);
            values.put("longitud", longitud);
            values.put("latitud", latitud);
            values.put("TipoLugar", tipolugar);

            id = db.insert(TABLE_LUGARES, null, values);
        }catch (Exception ex){
            ex.toString();
        }
        return id;

    }
    public ArrayList<Lugar> mostrarLugares(){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ArrayList<Lugar> listaLugares = new ArrayList<>();
        Lugar lugar = null;
        Cursor cursorLugares =null;
        cursorLugares = db.rawQuery("SELECT * FROM "+ TABLE_LUGARES, null);
        if (cursorLugares.moveToFirst()){
            do{
                lugar = new Lugar();
                lugar.setId(cursorLugares.getInt(0));
                lugar.setNombre(cursorLugares.getString(1));
                lugar.setDireccion(cursorLugares.getString(2));
                lugar.setHorario(cursorLugares.getString(3));
                //lugar.setPosicion(GeoPunto(cursorLugares.getDouble(5), cursorLugares.getDouble(6)));
                lugar.setHorario(cursorLugares.getString(7));
                listaLugares.add(lugar);

            }while (cursorLugares.moveToNext());
        }
        cursorLugares.close();
        return listaLugares;
    }

    public Lugar verLugar(int id){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();


        Lugar lugar = null;
        Cursor cursorLugares;

        cursorLugares = db.rawQuery("SELECT * FROM "+ TABLE_LUGARES + " WHERE id = "+ id + " LIMIT 1", null);
        if (cursorLugares.moveToFirst()){

                lugar = new Lugar();
                lugar.setId(cursorLugares.getInt(0));
                lugar.setNombre(cursorLugares.getString(1));
                lugar.setDireccion(cursorLugares.getString(2));
                lugar.setHorario(cursorLugares.getString(3));
                //lugar.setPosicion(GeoPunto(cursorLugares.getDouble(5), cursorLugares.getDouble(6)));
                lugar.setHorario(cursorLugares.getString(7));
        }
        cursorLugares.close();
        return lugar;
    }
}
