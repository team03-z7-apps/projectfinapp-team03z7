package com.example.bankapp_g03_z07.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper  extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "dbbankapp.db";
    public static final String TABLE_LUGARES = "tlugares";


    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL("CREATE TABLE " + TABLE_LUGARES + "(" + "id INTEGER PRIMARY KEY AUTOINCREMENT," +
              "nombre TEXT  NOT NULL," +
              "direccion TEXT NOT NULL," +
              "horario TEXT NOT NULL," +
              "estado INT," +
              "longitud DOUBLE NOT NULL," +
              "latitud DOUBLE NOT NULL," +
              "TipoLugar TEXT NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE" + TABLE_LUGARES);
        onCreate(db);
    }
}
