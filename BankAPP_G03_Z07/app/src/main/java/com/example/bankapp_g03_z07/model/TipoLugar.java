package com.example.bankapp_g03_z07.model;

import com.example.bankapp_g03_z07.R;

public enum TipoLugar {
    BANCOS ("Bancos", R.drawable.banco), CAJEROS ("Cajeros",R.drawable.cajero),
    CORRESPONSALES ("Corresponsales",R.drawable.corresponsal);

    private final String texto;
    private final int recurso;


    TipoLugar(String texto, int recurso) {
        this.texto = texto;
        this.recurso = recurso;
    }

    public String getTexto() {
        return texto;
    }

    public int getRecurso() {
        return recurso;
    }
}
