package com.example.bankapp_g03_z07.interfaz;

import com.example.bankapp_g03_z07.model.Lugar;
import com.example.bankapp_g03_z07.model.TipoLugar;

import java.util.ArrayList;
import java.util.List;

public class ListaLugares implements RepositorioLugares {

    public List<Lugar> lugaresLista;

    public ListaLugares() {
        lugaresLista = new ArrayList<Lugar>();
        //ejemplos();
    }

    @Override
    public Lugar elemento(int id) {

        return lugaresLista.get(id);
    }

    @Override
    public void agregar(Lugar lugar) {

        lugaresLista.add(lugar);
    }

    @Override
    public void eliminar(int id) {

        lugaresLista.remove(id);
    }

    @Override
    public void actualizar(int id, Lugar lugar) {

        lugaresLista.set(id, lugar);
    }

    @Override
    public int nuevo() {
        Lugar lugar = new Lugar();
        lugaresLista.add(lugar);
        return 0;
    }

    @Override
    public int cantidad() {

        return lugaresLista.size() - 1;
    }

/*    public void ejemplos(){
        agregar(new Lugar("El  Paraiso","Calle 16 #26-68", "www.elparaiso.com",
                4.6494, -74.0561, TipoLugar.BANCOS));
        agregar(new Lugar("Unicentro","Cra 4 #26-13", "www.unicentro.com",
                4.6148, -74.0667,TipoLugar.CAJEROS));
        agregar(new Lugar("Museo Nacional","Cra 7 #28-13", "www.munal.com",
                4.6159, -74.0681,TipoLugar.CORRESPONSALES));
        agregar(new Lugar("Museo Yopal","Cra 6 #12-23", "www.munarqq.com",
                4.5954, -74.0761,TipoLugar.CAJEROS));
    }*/
}
