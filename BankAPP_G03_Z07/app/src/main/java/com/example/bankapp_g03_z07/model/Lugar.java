package com.example.bankapp_g03_z07.model;

public class Lugar {
    private String nombre;
    private String direccion;
    private String horario;
    private boolean estado;
    private GeoPunto posicion;
    private TipoLugar tipo;
    private long fecha;

    public Lugar(String nombre, String direccion, String horario, boolean estado, double longitud,
                 double latitud, TipoLugar tipo) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.horario = horario;
        this.estado = estado;
        posicion = new GeoPunto(longitud, latitud);
        this.tipo = tipo;
        this.fecha = System.currentTimeMillis();
    }
    public Lugar() {
        posicion = new GeoPunto(0.0,0.0);
        fecha = System.currentTimeMillis();
        tipo = TipoLugar.BANCO;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public GeoPunto getPosicion() {
        return posicion;
    }

    public void setPosicion(GeoPunto posicion) {
        this.posicion = posicion;
    }

    public TipoLugar getTipo() {
        return tipo;
    }

    public void setTipo(TipoLugar tipo) {
        this.tipo = tipo;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Lugar{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", horario='" + horario + '\'' +
                ", estado=" + estado +
                ", posicion=" + posicion +
                ", tipo=" + tipo +
                ", fecha=" + fecha +
                '}';
    }
}
